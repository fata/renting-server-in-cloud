import sqlite3
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import collections

DECIMAL_ROUND = 4
TABLE_NAME = "Jobs"
NUMBER_OF_ROWS = 10000
SQLITE_FILE = "anon_jobs.db3"


def hist_plot(array, name):
    arr_max = int(array.max())
    arr_min = int(array.min())
    print(arr_max, arr_min)
    plt.title('Histogram of %s' % name)
    plt.hist(array, density=True, bins=100, range=(arr_min, arr_max+1))
    plt.savefig('plots/%s' % name)
    plt.close()


def density_list(array, name):
    length = array.size
    counter = collections.Counter(array)
    counter = collections.OrderedDict(sorted(counter.items(), key=lambda t: -t[0]))
    for key, value in counter.items():
        print key, round(value*1./length, DECIMAL_ROUND)


def interval_list(array, name):
    result = (array-array.shift()).fillna(0)
    print(result)


def plot_data(data_frame, columns):
    for column in columns:
        hist_plot(data_frame[column], column)


class Jobs:
    def __init__(self, arrival_times, run_times, sizes):
        self.arrival_times, self.run_times, self.sizes = (
            np.array(t) for t in zip(*sorted(zip(arrival_times, run_times, sizes)))
        )
        self.normalize_arrival_times()
        self.cumulative_run_times = np.cumsum(self.run_times)
        self.squared_run_times = np.square(self.run_times)
        self.cumulative_squared_run_times = np.cumsum(self.squared_run_times)

    def normalize_arrival_times(self):
        min_arrival = np.min(self.arrival_times)
        self.arrival_times = np.subtract(self.arrival_times, min_arrival)

    def run_time_window_variance(self, left, right):
        window_length = right-left + 1

        window_sum = self.cumulative_run_times[right] - self.cumulative_run_times[left] + self.run_times[left]
        window_mean = 1.*window_sum/window_length
        window_squared_sum = self.cumulative_squared_run_times[right] - self.cumulative_squared_run_times[left] + \
            self.squared_run_times[left]
        window_squared_mean = 1.*window_squared_sum/window_length
        variance = window_squared_mean - np.square(window_mean)
        return variance

    def plot_run_time_window_variances(self, window_size, name):
        result = []
        max_possible = self.arrival_times.max()-window_size
        last_index = self.arrival_times.searchsorted(max_possible, 'right')
        for i in range(last_index):
            end_of_window = self.arrival_times[i]+window_size
            end_index = self.arrival_times.searchsorted(end_of_window, 'right') - 1
            result.append(self.run_time_window_variance(i, end_index))
        indices = range(0, last_index)
        result, indices = (
            np.array(t) for t in zip(*sorted(zip(result, indices), reverse=True)))
        last_index_ten_percent = last_index/10
        ten_percent_removed = result[last_index_ten_percent:]
        plt.plot(ten_percent_removed)
        plt.savefig('plots/%s' % name)
        plt.close()
        return ten_percent_removed

    def plot_run_time_window_numbers(self, window_size, name):
        result = []
        max_possible = self.arrival_times.max()-window_size
        last_index = self.arrival_times.searchsorted(max_possible, 'right')
        for i in range(last_index):
            end_of_window = self.arrival_times[i]+window_size
            end_index = self.arrival_times.searchsorted(end_of_window, 'right') - 1
            result.append(end_index-i+1)
        result = np.array(sorted(result, reverse=True))
        plt.plot(result)
        plt.savefig('plots/%s' % name)
        plt.close()
        print(name)
        return result


# def main():
# job_table.sort_values(by=['ArrivalTime'])
# job_table['DeltaArrival'] = (job_table['ArrivalTime']-job_table['ArrivalTime'].shift()).fillna(0)
conn = sqlite3.connect(SQLITE_FILE)
c = conn.cursor()

# job_table = pd.read_sql_query("SELECT * FROM %s LIMIT %s" % (TABLE_NAME, NUMBER_OF_ROWS), conn)
job_table = pd.read_sql_query("SELECT * FROM %s" % TABLE_NAME, conn)

jobs = Jobs(job_table['SubmitTime'], job_table['RunTime'], job_table['NProc'])
# time_window_variances = np.array(jobs.plot_run_time_window_variances(100))
# window_sizes = [10, 20, 50, 100, 1000, 10*1000, 100*1000]
# for i in range(len(window_sizes)):
#     jobs.plot_run_time_window_numbers(window_sizes[i], 'Run Time Number W=%s' % window_sizes[i])

print(jobs.run_times.mean())
# print(np.corrcoef(jobs.arrival_times, jobs.run_times))
# print(time_window_variances)
# hist_plot(time_window_variances, 'time window variances')
# hist_plot(job_table["RunTime"], "submit time")
print(jobs.arrival_times.max()/3600)
